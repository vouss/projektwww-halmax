<?php

Route::get('/', function() {

	return View::make('pages.index');
});

Route::get('produkty', function() {

	return View::make('pages.produkty');
});
Route::get('produkty/blachitrap/tradycyjne', function() {
	return View::make('pages.products.tradycyjne');
});
Route::get('produkty/blachitrap/panelowe', function() {
	return View::make('pages.products.panelowe');
});
Route::get('produkty/blachitrap/narabek', function() {
	return View::make('pages.products.narabek');
});
Route::get('produkty/blachitrap/trapezy', function() {
	return View::make('pages.products.trapezy');
});
Route::get('produkty/dachowki/betonowe', function() {
	return View::make('pages.products.betonowe');
});
Route::get('produkty/dachowki/ceramiczne', function() {
	return View::make('pages.products.ceramiczne');
});
Route::get('produkty/orynnowanie/pcv', function() {
	return View::make('pages.products.pcv');
});
Route::get('produkty/orynnowanie/stalowe', function() {
	return View::make('pages.products.stalowe');
});
Route::get('produkty/akcesoria/membrany', function() {
	return View::make('pages.products.membrany');
});
Route::get('produkty/akcesoria/tasmy', function() {
	return View::make('pages.products.tasmy');
});
Route::get('produkty/akcesoria/akcesoria-montazowe', function() {
	return View::make('pages.products.akcesoria-montazowe');
});
Route::get('produkty/akcesoria/komunikacja', function() {
	return View::make('pages.products.komunikacja');
});
Route::get('firma', function() {

	return View::make('pages.firma');
});

Route::get('galeria', function() {

	return View::make('pages.galeria');
});

Route::get('promocje', function() {

	return View::make('pages.promocje');
});

Route::get('contact', function() {
	return View::make('pages.contact');
});

Route::post('contact', function() {
	//Get all the data and store it inside store variable
	$data = Input::all();

	//validation rules
	$rules = array(
		'first_name' => 'required|alpha',
		'last_name' => 'required|alpha',
		'phone_number' => 'numeric|min:9',
		'email' => 'required|email',
		'subject' => 'required|min:5',
		'message' => 'required|min:15'
		);

	$validator = Validator::make($data,$rules);
	if($validator ->passes()) {
		Mail::send('emails.contactmail',$data, function($message) use ($data) {
			$message->from($data['email'], $data['last_name']);
			$message->to('bosmarcin92@interia.pl', 'Marcin')->subject('Formularz kontaktowy');
		});
		$confirm = 'Wysłano formularz!';
		return View::make('pages.contact');
	} else {
		return Redirect::to('contact')->withInput()->withErrors($validator);
	}

});