<!DOCTYPE html>
<html>
<head>
	<title>Halmax Kielce</title>
	<meta charset="UTF-8">
    <meta name="description" content="Halmax">
    <meta name="author" content="Marcin Boś">    
    <meta name="viewport" content="width=device-width, maximum-scale=1.0">
    {{ HTML::script('theme/js/jquery-1.11.0.min.js') }}
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script><!--TODO / CHANGE-->
    <link href='http://fonts.googleapis.com/css?family=Bree+Serif&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    {{ HTML::style('theme/css/bootstrap.css') }}
    {{ HTML::style('theme/css/custom.css') }}
</head>
<body>
    @include('includes.nav')
	@yield('content')
	@include('includes.footer')
</body>
</html>