@extends('layouts.default')
@section('content')
<div class="container">
	<div class="page-header">
		<h1>Trapezy</h1>
	</div>
	<div class="row">
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail hgt">
  			<img src="{{asset('theme/img/trapezy/trapez.jpg')}}" id="std014" />
  			<div class="caption">
  				<hr />
  				<p class="lead">T-7</p>
  				<p>Blacha trapezowa T-7 (podbitka) polecana jest do okładzin elewacyjnych, wykończenia stropów oraz drzwi garażowych. Zapewnia efektowny wygląd zarówno wewnątrz, jak i na zewnątrz budynku.</p>
	  		</div>
		</div>
	</div>
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail hgt">
  			<img src="{{asset('theme/img/trapezy/trapez.jpg')}}" id="std015" />
  			<div class="caption">
  				<hr />
  				<p class="lead">T-12</p>
	   			<p>Blacha trapezowa T-12 ma szerokie zastosowanie tam, gdzie najważniejszymi czynnikami są umiarkowana cena i funkcjonalność materiału. Zapewnia efektowny i estetyczny wygląd domów jedno- i wielorodzinnych, budynków biurowych oraz pawilonów handlowych i usługowych.</p>
	  		</div>
		</div>
	</div>
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail hgt">
  			<img src="{{asset('theme/img/trapezy/trapez.jpg')}}" id="std016" />
  			<div class="caption">
  				<hr />
  				<p class="lead">T-18</p>
	   			<p>Blacha trapezowa T-18 NOWY posiada rowek kapilarny. Zastosowanie rowka kapilarnego zabezpiecza przed ewentualnym przeciekaniem trapezu na łączeniach. Polecany jest wszędzie tam, gdzie obok cech typowo funkcjonalnych i ceny istotna jest także estetyka pokrywanej powierzchni.</p>
	  		</div>
		</div>
	</div>
</div>
@stop