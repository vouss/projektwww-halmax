@extends('layouts.default')
@section('content')
<div class="container">
	<div class="page-header">
		<h1>Na rąbek</h1>
	</div>
	<div class="row">
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail hgt">
  			<img src="{{asset('theme/img/narabek/emka.jpeg')}}" id="std011" />
  			<div class="caption">
  				<hr />
  				<p class="lead">EMKA</p>
	   			<p>Plannja Emka 670 to doskonały materiał nadający się do budowy dachów oraz elewacji. Dzięki właściwościom plastycznym Plannja Emka 670 doskonale sprawdza się na najbardziej wyrafinowanych i skomplikowanych połaciach nowoczesnych oraz zabytkowych obiektów.</p>
	  		</div>
		</div>
	</div>
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail hgt">
  			<img src="{{asset('theme/img/narabek/emkaclick.jpeg')}}" id="std012" />
  			<div class="caption">
  				<hr />
  				<p class="lead">EMKA CLICK</p>
	   			<p>Plannja Emka Click 500 to nowoczesny panel dachowy o tradycyjnym kształcie blachy płaskiej na tzw. rąbek stojący. Klasyczny kształt sprawia, że panel Plannja Emka Click 500 może być stosowany podczas remontów obiektów zabytkowych oraz sakralnych.</p>
	  		</div>
		</div>
	</div>
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail hgt">
  			<img src="{{asset('theme/img/narabek/retro.png')}}" id="std013" />
  			<div class="caption">
  				<hr />
  				<p class="lead">RETRO</p>
	   			<p>Pladur® Wrinkle - mat gruboziarnisty, z kolorami wg znanych wzorców, system powlekania 2- warstwowy, grubość powłoki min. 35 mikronów</p>
	  		</div>
		</div>
	</div>
</div>
@stop