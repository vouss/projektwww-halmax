@extends('layouts.default')
@section('content')
<div class="container">
	<div class="page-header">
		<h1>Panelowe</h1>
	</div>
	<div class="row">
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail hgt">
  			<img src="{{asset('theme/img/panelowe/flex.jpeg')}}" id="std07" />
  			<div class="caption">
  				<hr />
  				<p class="lead">FLEX</p>
	   			<p>Plannja Flex posiada niepowtarzalny, cofnięty kąt przetłoczenia oraz wysokie 30 mm przetłoczenie z rowkiem w górnej fali dające wyjątkowy efekt wizualny. Załamania światła i cienie powstające w miejscach przetłoczeń sprawiają, że dach nabiera niepowtarzalnego charakteru.</p>
	  		</div>
		</div>
	</div>
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail hgt">
  			<img src="{{asset('theme/img/panelowe/germania.png')}}" id="std08" />
  			<div class="caption">
  				<br />
  				<br />
  				<hr />
  				<p class="lead">GERMANIA</p>
	   			<p>Blachy powlekane są najbardziej popularne wśród blaszanych pokryć dachowych. Obecnie stosuje się blachy ocynkowane, obustronnie ocynkowane antykorozyjną farbą gruntującą lub organiczną. </p>
	  		</div>
		</div>
	</div>
</div>
@stop