@extends('layouts.default')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<img src="{{asset('theme/img/logo/arcelormittal.jpg')}}" id="l1" />
		</div>
		<div class="col-md-4">
			<img src="{{asset('theme/img/logo/ssaab.png')}}" id="l2" />
		</div>
		<div class="col-md-4">
			<img src="{{asset('theme/img/logo/ThyssenKrupp.png')}}" id="l3" />
		</div>
	</div>
<div class="page-header">
	<h1>Tradycyjne</h1>
</div>
<div class="row">
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail hgt">
  			<img src="{{asset('theme/img/tradycyjne/royal.jpg')}}" id="std01" />
  			<div class="caption">
  				<hr />
  				<p class="lead">ROYAL</p>
	   			<p>Blachodachówka Royal szczególnie korzystnie prezentuje się na dachach o dużym kącie nachylenia. Wysoki profil tłoczenia w połączeniu z asymetrycznym kształtem dopełnia estetykę całego budynku.</p>
	  		</div>
		</div>
	</div>
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail hgt">
  			<img src="{{asset('theme/img/tradycyjne/scandic.jpg')}}" id="std02" />
  			<div class="caption">
  				<hr />
  				<p class="lead">SCANDIC</p>
	   			<p>Scandic 400 posiada wysokie tłocznie, tradycyjną 40 centymetrową długość modułów oraz łagodnie zaokrąglony profil nadający jej eleganckiego i niepowtarzalnego wyglądu. Cechy te sprawiają, że Scandic 400 korzystnie wpływa na estetykę dachów, zwłaszcza o większym kącie nachylenia.</p>
	  		</div>
		</div>
	</div>
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail hgt">
  			<img src="{{asset('theme/img/tradycyjne/smart.jpg')}}" id="std03" />
  			<div class="caption">
  				<hr />
  				<p class="lead">SMART</p>
	   			<p>Plannja Smart to blachodachówka charakteryzująca się subtelnym i symetrycznym profilem tłoczenia. Sprawdzającym się na wszystkich typach dachów. Atrakcyjna cena oraz wysoka jakość powodują, że blachodachówka Plannja Smart jest produktem ekonomicznym.</p>
	  		</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail hgt">
  			<img src="{{asset('theme/img/tradycyjne/diament.png')}}" id="std04" />
  			<div class="caption">
  				<hr />
  				<p class="lead">DIAMENT</p>
	   			<p>Dachówka blaszana przeznaczona do obiektów mieszkalnych oraz użyteczności publicznej. Doskonale sprawdza się na nowych oraz restaurowanych dachach. </p>
	  		</div>
		</div>
	</div>
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail hgt">
  			<img src="{{asset('theme/img/tradycyjne/joker.png')}}" id="std05" />
  			<div class="caption">
  				<hr />
  				<p class="lead">JOKER</p>
	   			<p>Dachówka blaszana, która ma zastosowanie w kryciu budynków mieszkalnych oraz użyteczności publicznej. Produkowane są w kilkunastu kolorach z powłoką poliester połysk lub mat ThyssenKrupp PLADUR®.</p>
	  		</div>
		</div>
	</div>
	<div class="col-sm-6 col-md-4">
		<div class="thumbnail hgt">		
  			<img src="{{asset('theme/img/tradycyjne/era.jpg')}}" id="std06" />
  			<div class="caption">
  				<br />
  				<br />
  				<br />
  				<hr />
  				<p class="lead">ERA</p>
  				<p>ERA Bratex Długość modułu: 300-400 mm Wysokość profilu: 63 mm Szerokość efektywnego krycia: 1005 mm Szerokość całkowita: 1133 mm</p>
	  		</div>
		</div>
	</div>
</div>

</div>
@stop