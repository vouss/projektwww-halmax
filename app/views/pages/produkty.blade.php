@extends('layouts.default')
@section('content')
<div class="container">
	<div class="row">
	<div class="col-md-4 well">
		<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Blachodachówki i trapezy</h3>
	  </div>
	  <div class="panel-body hgt">
	  <img src="{{asset('theme/img/main_product/image012.jpg')}}" id="prod_img0"/>
	  <hr />
	   <a href="{{ URL::to('produkty/blachitrap/tradycyjne') }}" class="btn btn-sm btn-block">TRADYCYJNE</a>
	   <a href="{{ URL::to('produkty/blachitrap/panelowe') }}" class="btn btn-sm btn-block">PANELOWE</a>
	   <a href="{{ URL::to('produkty/blachitrap/narabek') }}" class="btn btn-sm btn-block">NA RĄBEK</a>
	   <a href="{{ URL::to('produkty/blachitrap/trapezy') }}" class="btn btn-sm btn-block">TRAPEZY</a>
	  </div>
	</div>
	</div>
	<div class="col-md-4 well">
		<div class="panel panel-default">
	 		<div class="panel-heading">
	    		<h3 class="panel-title">Dachówki</h3>
	  		</div>
	  		<div class="panel-body hgt">
	  			<img src="{{asset('theme/img/main_product/image010.jpg')}}" id="prod_img1"/>
	  			<hr />
			  	<a href="{{ URL::to('produkty/dachowki/betonowe') }}" class="btn btn-sm btn-block">BETONOWE</a>
			  	<a href="{{ URL::to('produkty/dachowki/ceramiczne') }}" class="btn btn-sm btn-block">CERAMICZNE</a>
			</div>
		</div>
	</div>	
	<div class="col-md-4 well">
		<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Orynnowanie</h3>
	  </div>
	  <div class="panel-body hgt">
	  	<img src="{{asset('theme/img/main_product/image008.jpg')}}" id="prod_img2"/>
	  	<hr />
	   <a href="{{ URL::to('produkty/orynnowanie/pcv') }}" class="btn btn-sm btn-block">PCV</a>
	   <a href="{{ URL::to('produkty/orynnowanie/stalowe') }}" class="btn btn-sm btn-block">STALOWE</a>
	  </div>
	</div>
	</div>
</div>
	<div class="row">
	<div class="col-md-4 well">
		<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Akcesoria</h3>
	  </div>
	  <div class="panel-body hgt">
	  	<img src="{{asset('theme/img/main_product/image006.jpg')}}" id="prod_img3"/>
	  	<hr />
	   <a href="{{ URL::to('produkty/akcesoria/membrany') }}" class="btn btn-sm btn-block">MEMBRANY</a>
	   <a href="{{ URL::to('produkty/akcesoria/tasmy') }}" class="btn btn-sm btn-block">TAŚMY</a>
	   <a href="{{ URL::to('produkty/akcesoria/akcesoria-montazowe') }}" class="btn btn-sm btn-block">AKCESORIA MONTAŻOWE</a>
	   <a href="{{ URL::to('produkty/akcesoria/komunikacja') }}" class="btn btn-sm btn-block">KOMUNIKACJA</a>
	  </div>
	</div>
	</div>
	<div class="col-md-4 well">
		<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Okna</h3>
	  </div>
	  <div class="panel-body hgt">
	  	<img src="{{asset('theme/img/main_product/image003.jpg')}}" id="prod_img4"/>
	  	<hr />
	   <a href="http://www.fakro.pl/okna_dachowe/" class="btn btn-sm btn-block" target="_blank">FAKRO</a>
	   <a href="http://www.velux.pl/klienci/produkty/okna_dachowe" class="btn btn-sm btn-block" target="_blank">VELUX</a>
	   <a href="http://www.okpol.pl/www_okpol/index.php?option=com_content&view=article&id=64&Itemid=116&lang=pl-PL" class="btn btn-sm btn-block" target="_blank">OKPOL</a>
	  </div>
	</div>
	</div>	<div class="col-md-4 well">
		<div class="panel panel-default">
	  <div class="panel-heading">
	    <h3 class="panel-title">Narożniki, profile, listwy</h3>
	  </div>
	  <div class="panel-body hgt">
	   <img src="{{asset('theme/img/main_product/image002.jpg')}}" id="prod_img5"/>
	   <hr />
	  </div>
	</div>
	</div>
</div>
</div>
@stop