@extends('layouts.default')
@section('content')
	<div class="container">
		<div class="well">
			<div class="row">
				<div class="col-md-4">
		            {{ Form:: open(array('url' => 'contact')) }} 
		                        <div class="form-group">
									{{ Form:: label ('first_name', 'Imię*', array('for' => 'name')) }}
									{{ Form:: text ('first_name', '',array('class' => 'form-control', 'placeholder' => 'Imię', 'required' => 'required')) }}
								</div>
								<div class="form-group">
									{{ Form:: label ('last_name', 'Nazwisko*', array('for' => 'name')) }}
									{{ Form:: text ('last_name', '', array('class' => 'form-control', 'placeholder' => 'Nazwisko', 'required' => 'required')) }}
								</div>
								<div class="form-group">
									{{ Form:: label ('phone_number', 'Telefon*' )}}
									<div class="input-group">
										<span class="input-group-addon"><span class="glyphicon glyphicon-earphone"></span></span>	
										{{ Form:: text ('phone_number', '', array('class' => 'form-control', 'placeholder' => 'XXXXXXXXX', 'required' => 'required')) }}
									</div>
								</div>
								<div class="form-group">
									{{ Form:: label ('email', 'Adres E-mail*', array('for' => 'email')) }}
		                        	<div class="input-group">
		                            	<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
										{{ Form:: email ('email', '', array('class' => 'form-control', 'placeholder' => 'E-mail', 'required' => 'required')) }}
									</div>
								</div>
						@if($errors->has())
		            		<div class="alert alert-warning alert-dismissable">
  								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								{{ HTML::ul($errors->all(), array('class' => 'errors')) }}
		            		</div>
		            	@endif
				</div>
				<div class="col-md-4">
		            <div class="form-group">
		            	<div class="form-group">
							{{ Form:: label ('subject', 'Temat', array('for' => 'subject')) }}
							{{ Form:: text ('subject', '', array('class' => 'form-control', 'placeholder' => 'Temat', 'required' => 'required')) }}
						</div>
		            	{{ Form:: label ('message', 'Wiadomość*', array('for' => 'name') )}}
						{{ Form:: textarea ('message', '', array('class' => 'form-control', 'rows' => '9', 'cols' => '25', 'required' => 'required'))}}
		            </div>
		            <div class="form-group">
      					<div class="col-lg-4">
      					    {{ Form::submit('Wyślij', array('class' => 'btn btn-primary')) }}
      					</div>
      					<div class="col-lg-8">
							{{ Form::reset('Wyczyść', array('class' => 'btn btn-default')) }}
							{{ Form::close() }}
      					</div>
    				</div>
		   		</div>

				<div class="col-md-4">
			        <form>
			            <legend>Biuro</legend>
			            <address>
			                <strong>Z.H.U. HALMAX Kawecki Jakub</strong><br>
			                Chańcza 31A, 26-035 Raków<br>
			            </address>
			            <address>
			            	<strong>Tel. komórkowy</strong><br /> 501 774 368 <br />
			            	<strong>Tel./Fax</strong><br /> (41) 342 51 22 
			            </address>
			            <address>
			                <strong>E-mail</strong><br>
			                <a href="mailto:biurokielce@halmax.pl">biurokielce@halmax.pl</a>
			            </address>
			            <legend>Punkt odbioru</legend>
			            <address>
			                Domaszowice 67A, 25-255 Kielce<br>
			                <a href="mailto:zamowienia@halmax.pl">zamowienia@halmax.pl</a>
			            </address>
			        <form>
			    </div>
			</div>
		</div>
@stop