@extends('layouts.default')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<img src="{{asset('theme/img/logo_1.jpg')}}" id="logo0" />
				<div class="page-header">Coś o firmie...</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
			<div class="col-md-6">
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
				  </ol>

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner">
				    <div class="item active">
						<img src="{{asset('theme/img/slider/img03.jpg')}}" id="logo3" />
				    </div>
				    <div class="item">
						<img src="{{asset('theme/img/slider/img02.jpg')}}" id="logo2" />
				    </div>
				  </div>
				 </div>
			</div>
		</div>
	</div>
@stop