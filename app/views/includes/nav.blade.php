<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">HALMAX</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
		<li><a href="{{ URL::to('/') }}">STRONA GŁÓWNA</a></li>
		<li><a href="{{ URL::to('produkty') }}">PRODUKTY</a></li>
		<li><a href="{{ URL::to('firma') }}">O NAS</a></li>
		<li><a href="{{ URL::to('galeria') }}">GALERIA</a></li>
		<li><a href="{{ URL::to('promocje') }}">PROMOCJE</a></li>
		<li><a href="{{ URL::to('contact') }}">KONTAKTY</a></li>
      </ul>
    </div>
  </div>
</nav>
